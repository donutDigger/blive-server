<?php

$db = new PDO('sqlite:automation.sqlite');

// if (@$_POST['zoneName'] != '') {
//   $db->query("INSERT INTO zone(name, category, id_room, id_device) VALUES(
//     '" . $_POST['zoneName'] . "',
//     '" . $_POST['zoneCategory'] . "', 
//     '" . $_POST['roomId'] . "',
//     '" . $_POST['deviceId'] . "'
//     )");
// }

// $querySelectZone = "SELECT zone.id as zoneid, 
//                zone.name as zonename,
//                zone.category as zonecategory, 
//                zone.id_room as zoneidroom, 
//                zone.id_device as zoneiddevice, 
//                room.name as roomname,
//                device.name as devicename,
//                device.type as devicetype,
//                device.ip_address as deviceip
//             FROM zone, room, device WHERE 
//                  zone.id_room = room.id AND
//                  zone.id_device = device.id";

$querySelectRoom   = "SELECT * FROM room";
$querySelectDevice = "SELECT * FROM device";

?>

<div ng-show="contentSettingZone" class="box box-widget table-responsive" style="padding: 5px">  
  <table class="table table-bordered table-striped data-tables-1">
    <thead>
    <tr>
      <th>No</th>
      <th>Name</th>
      <th>Room</th>
      <th>Controller</th>
      <th>Type</th>
      <th>Category</th>
      <th></th>
    </tr>
    </thead>
    <tbody>
      <tr ng-repeat="x in values">
          <td></td>
          <td>{{ x.zonename }}</td>
          <td>{{ x.roomname }}</td>
          <td>{{ x.devicename }}</td>
          <td>{{ x.devicetype }}</td>
          <td>{{ x.zonecategory }}</td>
          <td>
            <a href="javascript:void(0)" class="btn btn-default btn-flat btn-xs"><i class="fa fa-pencil"></i> Edit</a>
            <a href="javascript:void(0)" class="btn btn-default btn-flat btn-xs"><i class="fa fa-trash"></i> Delete</a> 
          </td>
      </tr>
    </tbody>
  </table>
</div>