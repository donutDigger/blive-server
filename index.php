<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>BE | Bio Experience</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Sweet Alert -->
  <script src="bower_components/sweet/sweetalert-dev.js"></script>
  <link rel="stylesheet" href="bower_components/sweet/sweetalert.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="bower_components/Ionicons/css/ionicons.min.css">
  <!-- DataTables -->
  <link rel="stylesheet" href="bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="dist/css/skins/_all-skins.min.css">
  <!-- AngularJS -->
  <script src="bower_components/angular/angular.min.js"></script>
  <script src="bower_components/angular/angular-route.js"></script>

  <!-- Google Font -->
  <!-- <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic"> -->
</head>

<!-- ADD THE CLASS layout-boxed TO GET A BOXED LAYOUT -->
<body ng-app="automationApp" class="hold-transition skin-black-light layout-boxed sidebar-mini">
<!-- <div id="url_loader" style="display: none"></div> -->
<!-- Site wrapper -->
<div class="wrapper">

  <?php require('head.html'); ?>
  <?php require('leftSidebar.html'); ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        {{contentTitle}}
        <small>{{contentSubtitle}}</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href=""><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">{{contentTitle}}</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div ng-view></div>
        </div>
      </div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <?php require('footer.html'); ?>
  <?php require('controlSidebar.html'); ?>

</div>
<!-- ./wrapper -->

<?php require('scripts.html'); ?>

</body>
</html>
