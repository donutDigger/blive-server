/* CONTROLLER CTRL */
  app.controller("controllerCtrl", function ($scope, $http) {
    /* Load Json then put to the table */
    $http.get("loadJsonController.php").then(function (response) {$scope.values = response.data; });

    $scope.submitNewController = function() {
      $http.get("actionAddController.php?name=" + $scope.controllerNewName + 
                "&type=" + $scope.controllerNewType + 
                "&ip=" + $scope.controllerNewIp)
      .then(function (response) { });

      /* Load Json then put to the table */
      $http.get("loadJsonController.php").then(function (response) {$scope.values = response.data; });

      /* Dismiss Modal */
      $("[data-dismiss=modal]").trigger({ type: "click" });
    }

    $scope.submitEditController = function() {
      /* Update Controller values */
      $http.get("actionEditController.php?id=" + $scope.controllerEditId + 
                "&name=" + $scope.controllerEditName + 
                "&type=" + $scope.data.selectedOptionType.value + 
                "&ip=" + $scope.controllerEditIp)
      .then(function (response) { });

      /* Load Json then put to the table */
      $http.get("loadJsonController.php").then(function (response) {$scope.values = response.data; });

      /* Dismiss Modal */
      $("[data-dismiss=modal]").trigger({ type: "click" });
    }

    $scope.editController = function(id, name, type, ip) { 
      $scope.controllerEditId   = id;
      $scope.controllerEditName = name;
      $scope.controllerEditIp   = ip;
      $scope.data = {
        availableOptions: [
          {value: 'onof', name: 'On/Off'},
          {value: 'dimmerac', name: 'Dimmer AC'},
          {value: 'dimmerdc', name: 'Dimmer DC'},
          {value: 'ir', name: 'Infrared'},
          {value: 'rf', name: 'RF'},
          {value: 'cctv', name: 'CCTV'}
        ],
        selectedOptionType: {value: type}
      };
    }

    $scope.deleteController = function(x) {
      /* Delete Controller values */
      $http.get("actionDeleteController.php?id=" + x)
      .then(function (response) { });

      /* Load Json then put to the table */
      $http.get("loadJsonController.php").then(function (response) {$scope.values = response.data; });
    }
    
  });