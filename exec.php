<?php

$idZone	 = $_GET['idZone'];
$command = $_GET['command'];

$db = new PDO('sqlite:automation.sqlite');

$querySelectZone = "SELECT 	 zone.type as zonesort,
							 device.type as devicetype,
							 device.ip_address as deviceip,
							 zone.command as zonecommand
					  FROM zone, device WHERE zone.id_device = device.id AND
					  		 zone.id = '$idZone'";

foreach ($db->query($querySelectZone) as $key => $row) {
	$zoneCommand  = $row["zonecommand"];
	$zoneIp 	  = $row["deviceip"];
	$zoneType 	  = $row["devicetype"];
	$zoneSort 	  = $row["zonesort"];
}

// echo "id: " . $zoneId . "<br>";
// echo "cat: " . $zoneCategory . "<br>";
// echo "ip: " . $zoneIp . "<br>";
// echo "type: " . $zoneType . "<br>";
// echo "command: " . $zoneCommand . "<br>";
// echo "sort: " . $zoneSort . "<br>";

switch ($zoneType) {
	case 'onof':
		// echo $zoneSort;
		// file_get_contents("http://" . $zoneIp . "/" . $zoneCommand . "/?value=" . $command);
		switch ($zoneSort) {
			case 'light':
				file_get_contents("http://" . $zoneIp . "/" . $zoneCommand . "/?value=" . $command);
				if ($command == "0") {
					$queryEdit = "UPDATE zone SET status = '0' WHERE id = '$idZone'";
				} else {
					$queryEdit = "UPDATE zone SET status = '1' WHERE id = '$idZone'";
				}
				$db->query($queryEdit);

				break;
			case 'wh':
				file_get_contents("http://" . $zoneIp . "/WH/" . $zoneCommand . "/?value=" . $command);
				if ($command == "0") {
					$queryEdit = "UPDATE zone SET status = '0' WHERE id = '$idZone'";
				} else {
					$queryEdit = "UPDATE zone SET status = '1' WHERE id = '$idZone'";
				}
				$db->query($queryEdit);
				// echo "http://" . $zoneIp . "/WH/" . $zoneCommand . "/?value=" . $command;
				break;
			
			default:
				# code...
				break;
		}
		break;
	case 'dimmerac':
		file_get_contents("http://" . $zoneIp . "/" . $zoneCommand . "/?value=" . $command);
		if ($command == "0") {
			$queryEdit = "UPDATE zone SET status = '0' WHERE id = '$idZone'";
		} else {
			$queryEdit = "UPDATE zone SET status = '1' WHERE id = '$idZone'";
		}
		$db->query($queryEdit);

		break;
	case 'ir' :
		switch ($zoneSort) {
			case 'ac':
				// echo "Return : " . file_get_contents("http://" . $zoneIp . "/AC/" . $zoneCommand . "=".$command);
				echo "Return : " . file_get_contents("http://" . $zoneIp . "/AC/" . $zoneCommand . "=command_".$command);
				if ($command == "5") {
					$queryEdit = "UPDATE zone SET status = '0' WHERE id = '$idZone'";
				} else {
					$queryEdit = "UPDATE zone SET status = '1' WHERE id = '$idZone'";
				}
				$db->query($queryEdit);
				break;
			case 'tv':
				echo "Return : " . file_get_contents("http://" . $zoneIp . "/TV/" . $zoneCommand . "/?value=" . $command);
				if ($command == "0") {
					$queryEdit = "UPDATE zone SET status = '0' WHERE id = '$idZone'";
				} else {
					$queryEdit = "UPDATE zone SET status = '1' WHERE id = '$idZone'";
				}
				$db->query($queryEdit);
				break;
			case 'wh':
				file_get_contents("http://" . $zoneIp . "/WH/" . $zoneCommand . "/?value=" . $command);
				if ($command == "0") {
					$queryEdit = "UPDATE zone SET status = '0' WHERE id = '$idZone'";
				} else {
					$queryEdit = "UPDATE zone SET status = '1' WHERE id = '$idZone'";
				}
				$db->query($queryEdit);
			default:
				# code...
				break;
		}
		break;
	case 'rf' :
		switch ($zoneSort) {
			case 'wh':
				$command = explode(',', $zoneCommand)[$command-1];
				file_get_contents("http://192.168.168.113:8080/tesSerial2.php?serialData=" . $command);
				if ($command == "2") {
					$queryEdit = "UPDATE zone SET status = '0' WHERE id = '$idZone'";
				} else {
					$queryEdit = "UPDATE zone SET status = '1' WHERE id = '$idZone'";
				}
				$db->query($queryEdit);
				break;
		break;
		}
	default:
		// code...
		break;
}


?>