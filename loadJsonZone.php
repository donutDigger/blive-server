<?php

$db = new PDO('sqlite:automation.sqlite');

$querySelectZone = "SELECT zone.id as zoneid, 
							 zone.name as zonename,
							 zone.category as zonecategory, 
							 zone.id_room as zoneidroom, 
							 zone.id_device as zoneiddevice, 
							 zone.type as zonesort,
							 zone.status as zonestatus,
							 room.name as roomname,
							 device.name as devicename,
							 device.type as devicetype,
							 device.ip_address as deviceip,
							 zone.command as zonecommand
					  FROM zone, room, device WHERE 
					  		 zone.id_room = room.id AND
					  		 zone.id_device = device.id";

// $varArray = array();

// foreach ($db->query($querySelectZone) as $key => $row) {
// 	$varArray[$key] = $row;
// }

// print_r(json_encode($varArray));

foreach($db->query($querySelectZone) as $row) {
	$customer[] = $row;
}

// while ($row = $db->query($querySelectZone)) {
//     $customer[] = $row;
// }

// print_r($customer);

$struct = array("Devices" => $customer);
print json_encode($struct);

?>