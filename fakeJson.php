<?php

$db = new PDO('sqlite:automation.sqlite');

$querySelectZone = "SELECT zone.id as zoneid, 
							 zone.name as zonename,
							 zone.category as zonecategory, 
							 zone.id_room as zoneidroom, 
							 zone.id_device as zoneiddevice, 
							 room.name as roomname,
							 device.name as devicename,
							 device.type as devicetype,
							 device.ip_address as deviceip
					  FROM zone, room, device WHERE 
					  		 zone.id_room = room.id AND
					  		 zone.id_device = device.id";

$querySelectRoom   = "SELECT * FROM room";
$querySelectDevice = "SELECT * FROM device";

$varArray = array();

foreach ($db->query($querySelectZone) as $key => $row) {
	$varArray[$key] = $row;
}

print_r(json_encode($varArray));

?>