/* ROOM CTRL */
  app.controller("roomCtrl", function ($scope, $http) {
  	/* Load Json then put to the table */
    $http.get("loadJsonRoom.php").then(function (response) {$scope.values = response.data; });

    $scope.submitNewRoom = function() {
      $http.get("actionAddRoom.php?name=" + $scope.roomNewName + 
                "&password=" + $scope.roomNewPassword)
      .then(function (response) { });

      /* Load Json then put to the table */
      $http.get("loadJsonRoom.php").then(function (response) {$scope.values = response.data; });

      /* Dismiss Modal */
      $("[data-dismiss=modal]").trigger({ type: "click" });
    }

    $scope.editRoom = function(id, name, password) { 
      $scope.roomEditId   	  = id;
      $scope.roomEditName 	  = name;
      $scope.roomEditPassword = password;
    }

    $scope.submitEditRoom = function() {
      /* Update Room values */
      $http.get("actionEditRoom.php?id=" + $scope.roomEditId + 
                "&name=" + $scope.roomEditName + 
                "&password=" + $scope.roomEditPassword)
      .then(function (response) { });

      /* Load Json then put to the table */
      $http.get("loadJsonRoom.php").then(function (response) {$scope.values = response.data; });

      /* Dismiss Modal */
      $("[data-dismiss=modal]").trigger({ type: "click" });
    }

    $scope.deleteRoom = function(x) {
      /* Delete Controller values */
      $http.get("actionDeleteRoom.php?id=" + x)
      .then(function (response) { });

      /* Load Json then put to the table */
      $http.get("loadJsonRoom.php").then(function (response) {$scope.values = response.data; });
    }

  });