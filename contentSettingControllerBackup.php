<?php

$db = new PDO('sqlite:automation.sqlite');

if (@$_POST['name'] != '') {
  $db->query("INSERT INTO device(name, type, ip_address) VALUES(
    '" . $_POST['name'] . "', 
    '" . $_POST['type'] . "',
    '" . $_POST['ipAddress'] . "'
    )");
}

$querySelectDevice = "SELECT * FROM device";

?>

<div ng-show="contentSettingController">
  <div style="width: 100%; margin-bottom: 5px;text-align: center">
    <button class="btn btn-default btn-flat" data-toggle="modal" data-target="#modal-add-new-controller"><i class="fa fa-plus"></i> Add new</button>
  </div>

  <div class="box box-widget table-responsive" style="padding: 5px">
    <table class="table table-bordered table-striped data-tables-1">
      <thead>
      <tr>
        <th>No</th>
        <th>Name</th>
        <th>Type</th>
        <th>IP Address</th>
        <th></th>
      </tr>
      </thead>
      <tbody>
      <?php $no=1; foreach($db->query($querySelectDevice) as $row){ ?>
        <tr>
          <td><?= $no ?></td>
          <td><?= $row['name'] ?></td>
          <td><?= $row['type'] ?></td>
          <td><?= $row['ip_address'] ?></td>
          <td>
            <a href="edit_device.php?id=<?= $row['id'] ?>" class="btn btn-default btn-flat btn-xs"><i class="fa fa-pencil"></i> Edit</a>
            <a href="delete_device.php?id=<?= $row['id'] ?>" class="btn btn-default btn-flat btn-xs"><i class="fa fa-trash"></i> Delete</a> 
          </td>
        </tr>
      <?php $no++; } ?>
      </tbody>
    </table>
  </div>
</div>

<div class="modal fade" id="modal-add-new-controller">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Add New Controller</h4>
      </div>
      <div class="modal-body">
        <form method="post" action="" class="">
          <p>
            Controller Name :
            <input type="text" class="form-control" name="name" placeholder="Controller Name" required="required">
          </p>

          <p>
            Type :
            <select name="type" class="form-control">
              <option value="onof">On/Off</option>
              <option value="dimmerac">Dimmer AC</option>
              <option value="dimmerdc">Dimmer DC</option>
              <option value="ir">IR</option>
            </select>
          </p>

          <p>
            IP Address :
            <input type="text" class="form-control" name="ipAddress" placeholder="IP Address" required="required">
          </p>
          <!-- <input type="submit" name="submit" value="submit" class="btn btn-primary pull-right"> -->
      </div>
      <div class="modal-footer">
          <button type="button" class="btn btn-default pull-left btn-flat" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary btn-flat"><i class="fa fa-send"></i> Submit</button>
        </form>
      </div>
    </div>
  </div>
</div>