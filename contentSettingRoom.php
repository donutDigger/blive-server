<?php

$db = new PDO('sqlite:automation.sqlite');

if (@$_POST['name'] != '') {
  $db->query("INSERT INTO room(name, password) VALUES(
    '" . $_POST['name'] . "', 
    '" . $_POST['password'] . "'
    )");
}

$querySelectRoom = "SELECT * FROM room";

?>

<div ng-show="contentSettingRoom" class="box box-widget table-responsive" style="padding: 5px">  
  <table class="table table-bordered table-striped data-tables-1" width="100%" cellspacing="0">
    <thead>
    <tr>
      <th>No</th>
      <th>Name</th>
      <th></th>
    </tr>
    </thead>
    <tbody>
    <?php $no=1; foreach($db->query($querySelectRoom) as $row){ ?>
      <tr>
        <td><?= $no ?></td>
        <td><?= $row['name'] ?></td>
        <td>
          <a href="edit_room.php?id=<?= $row['id'] ?>" class="btn btn-default btn-flat btn-xs"><i class="fa fa-pencil"></i> Edit</a>
          <a href="delete_room.php?id=<?= $row['id'] ?>" class="btn btn-default btn-flat btn-xs"><i class="fa fa-trash"></i> Delete</a> 
        </td>
      </tr>
    <?php $no++; } ?>
    </tbody>
  </table>
</div>
