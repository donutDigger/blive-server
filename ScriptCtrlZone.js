/* ZONE CTRL */
  app.controller("zoneCtrl", function ($scope, $http) { //response.data[0].name
    var isRF = false;
  	/* Load Json then put to the table */
    $http.get("loadJsonZone.php").then(function (response) {$scope.values = response.data["Devices"]; });

    $http.get("loadJsonRoom.php").then(function (response) {$scope.objRoom = response.data; });
    $http.get("loadJsonController.php").then(function (response) {$scope.objController = response.data; });

    $scope.submitNewZone = function() {
      /* Check if RF checked */
      if (isRF) {
        // alert("commandnya rf");
        $scope.zoneNewCommand = $('#txt-command-rf-0').val() + ',' + $('#txt-command-rf-1').val();
      } else {
        // alert("commandnya bukan rf");
      }
      $http.get("actionAddZone.php?name=" + $scope.zoneNewName + 
                // "&category=" + $scope.zoneNewCategory + 
                "&roomId=" + $scope.zoneNewRoomId +
                "&type=" + $scope.zoneNewType +
                "&deviceId=" + $scope.zoneNewControllerId +
                "&command=" + $scope.zoneNewCommand)
      .then(function (response) { });
      // alert($scope.zoneNewRoom + ", " + $scope.zoneNewController);
      // alert($scope.zoneNewCommand);

      /* Load Json then put to the table */
      $http.get("loadJsonZone.php").then(function (response) {$scope.values = response.data["Devices"]; });

      /* Dismiss Modal */
      $("[data-dismiss=modal]").trigger({ type: "click" });
    }

    $scope.submitEditZone = function() {
      /* Update Controller values */
      $http.get("actionEditZone.php?zoneId=" + $scope.zoneEditId + 
                "&zoneName=" + $scope.zoneEditName + 
                "&roomId=" + $scope.zoneEditRoomId + 
                "&zoneType=" + $scope.zoneEditType + 
                "&zoneCommand=" + $scope.zoneEditCommand + 
                "&deviceId=" + $scope.zoneEditControllerId)
      .then(function (response) { });

      /* Load Json then put to the table */
      $http.get("loadJsonZone.php").then(function (response) {$scope.values = response.data["Devices"]; });

      /* Dismiss Modal */
      $("[data-dismiss=modal]").trigger({ type: "click" });
    }

    $scope.editZone = function(id, name, zoneIdRoom, controllerName, controllerType, category) { 
      $scope.zoneEditId   = id;
      $scope.zoneEditName   = name;
    }

    $scope.deleteZone = function(x) {
      /* Delete Controller values */
      $http.get("actionDeleteZone.php?id=" + x)
      .then(function (response) { });

      /* Load Json then put to the table */
      $http.get("loadJsonZone.php").then(function (response) {$scope.values = response.data["Devices"]; });
    }

    $('#device-option-new').change(function () {
      var optionSelected = $(this).find("option:selected");
      var valueSelected  = optionSelected.val();
     
      $(".commands-option").hide();

      switch(valueSelected) {
        case "ac":
          $("#p-select-option-ac").show();
          break;
        case "tv":
          $("#p-select-option-tv").show();
          break;
        case "light":
          $("#p-select-option-gpio").show();
          break;
        case "wh":
          $("#p-select-option-wh").show();
          break;
      }
    });

    $('#device-option-edit').change(function () {
      var optionSelected = $(this).find("option:selected");
      var valueSelected  = optionSelected.val();
     
      $(".commands-option").hide();

      switch(valueSelected) {
        case "ac":
          $("#p-select-option-ac-edit").show();
          break;
        case "tv":
          $("#p-select-option-tv-edit").show();
          break;
        case "light":
          $("#p-select-option-gpio-edit").show();
        break;
      }
    });

    $('#cb-rf-wh').click(function() {
        var self = this;
        if (self.checked) {
          // alert("false");
          isRF = true;
          $("#select-option-wh-rf").show();
          $("#select-option-wh-not-rf").hide();
        } else {
          // alert("true");
          isRF = false;
          $("#select-option-wh-rf").hide();
          $("#select-option-wh-not-rf").show();
        } 
    });

  });

  